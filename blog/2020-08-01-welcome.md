---
id: welcome
title: Welcome
author: vguerand
author_title: Developer
author_url: vguerand.com
author_image_url: https://avatars3.githubusercontent.com/u/33958228?s=400&u=a3bebad55789739907435a98b7f7470369fc833a&v=4
tags: [beta, hello]
---

Bienvenue sur ce projet citoyen !
Après 5 mois de travail, nous sommes heureux de vous présenter la première brique du projet.

Cette brique a été conçue avec d'anciens étudiants de l'école 42.
@vguerand && @mtagaizi &&  @smokthar &&  @ppichier
