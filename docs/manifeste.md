---
id: manifeste
title: Manifeste pour un débat public ouvert
sidebar_label: Manifeste
---

### Principes et philosophie  
 
---

## Article 1 – Débat Public
 Le débat public doit permettre :

- d'informer le public sur l’opportunité, les caractéristiques et objectifs du projet ;
- d'assurer l’expression la plus large possible du public à l’aide de différents supports ;
- d'éclairer le maître d’ouvrage par de nouveaux éléments d’appréciations.
 
---

## Article 2 – Débat public ouvert
Le débat public “ouvert” est un système d'information et de participation et un mode d’organisation démocratique de la société qui :

- Garantit la transparence des choix politiques et l’intégrité des décideurs,
- Permet la participation de tous aux prises de décisions communes,
- Accroît le pouvoir d’agir des citoyens, les logiques collaboratives et pair à pair dans l’action publique et d’intérêt général.
 
---

## Article 3 – Citoyenneté active
Dans le débat public , chacun peut être citoyen actif et a la liberté et les moyens effectifs de participer à définir les règles qui régissent le fonctionnement de la société (droits et devoirs), en accord avec l’article 6 de la déclaration des droits de l’Homme et du Citoyen : “La loi est l’expression de la volonté générale. Tous les citoyens ont droit de concourir personnellement, ou par leurs représentants, à sa formation”.

---

## Article 4 – Technologies civiques
Pour co-construire les règles communes, un débat public ouvert s’appuie sur les potentiels humains et technologiques, dont elle favorise la connaissances par tous et l’implémentation dans tous les territoires. Un débat public ouvert organise aussi la participation et l’engagement des citoyens dans des actions et des projets concrets, en s’appuyant sur les technologies de l’information et de la communication pour créer des espaces publics de partage et de coopération.

---

## Article 5 – Valeurs
Le débat public donne accès aux données et services essentiels dans la liberté, l’équité et la gratuité du service public.
 
---

## Article 6 – Principes
Le débat public ouvert est complémentaire d’une démocratie représentative, dont elle cherche à améliorer le fonctionnement par plus de transparence, de participation et de collaboration.
 
---

## Article 7 – Nos engagements
Pour appliquer les principes d’un débat public ouvert, les signataires de cette charte s’engagent collectivement et individuellement à :

##### 1. Soutenir les projets d’innovation démocratique

Au sein du collectif, les organisations publiques ou privées, les citoyens, chercheurs, élus, journalistes et experts membres du débat public agissent en soutien aux innovations démocratiques, pour les aider, les renforcer, les accélérer, les questionner, les évaluer, leur offrir des terrains d’expérimentation, les cartographier, les faire connaître, etc.

##### 2. Faire vivre la communauté des transformateurs démocratiques

Le débat public structure, alimente et fait vivre la communauté à travers l’organisation d’échanges, de rencontres, d’événements, de formations, d’accompagnements, d’expérimentations ou de recherche-action dédiés spécifiquement aux innovateurs démocratiques.

##### 3. Diffuser et médiatiser les projets et la vision du débat public

Le débat public et ses membres documentent les actions de la communauté des innovateurs démocratiques et communiquent sur internet, auprès des médias et des décideurs (plaidoyer citoyen) pour accroître la présence de la question démocratique dans le débat public et politique.
 
---

## Article 8 – Le collectif débat public
Ledébat public rassemble des personnes et des organisations qui promeuvent et mettent en oeuvre les principes du “gouvernement ouvert” : gouvernance transparente, participative, collaborative.

Le collectif regroupe une large diversité de citoyennes et citoyens : des porteurs de projets, des associations, des start-up et technologies civiques (CivicTech), des collectifs citoyens, des élus, des chercheurs, des agents publics, des entrepreneurs, des journalistes…

Les porteurs de projets qui mettent en œuvre des solutions pour améliorer les pratiques ou systèmes de décision et d’action politiques sont au cœur du collectif. La communauté se structure autour d’eux, en soutien à ces acteurs de la transformation démocratique.

---
 
## Article 9 – Un collectif non-partisan
Le débat public n’est pas un parti et n’est affilié à aucun mouvement, parti politique, institution publique ou organisation privée. Le collectif ne soutient pas de structures partisanes ni de candidat à aucune élection. Il ne se revendique d’aucune couleur politique. En ce sens, débat public est un collectif non-partisan.

---

## Article 10 – Un collectif transpartisan
Le débat public rassemble des citoyens dans leur diversité, valorise la pluralité des regards et des expériences. Le collectif accueille des élus, des militants, dès lors qu’ils adhèrent à titre personnel, sans esprit d’entrisme ni de prosélytisme, qu’ils n’utilisent pas leur qualité de membre du débat public dans leurs engagements politiques extérieurs. Les instances dirigeantes du collectif veillent, dans leur composition, à poursuivre un objectif d’équilibre et de neutralité. En ce sens, débat public est un collectif transpartisan.

---

## Article 11 – un collectif indépendant
Le collectif débat public est porté par une association à but non lucratif (loi 1901), financée par ses membres et par une diversité de partenaires et organisations, publiques comme privées. La gouvernance et l’administration du collectif sont dissociées de son financement. En ce sens, débat public est un collectif indépendant.
 
---

## Article 12 – un collectif engagé
L’idéal défendu par le débat public consiste à tendre vers un système politique capable d’activer les capacités collectives maximales d’un maximum de citoyens à participer à un maximum d’actions d’intérêt général sur un maximum de sujets. Cette vision du débat public guide les actions et ses prises de position. En ce sens, le collectif le débat public est un collectif engagé.

 

 