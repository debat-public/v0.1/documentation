---
id: installation
title: Manuel installation
sidebar_label: Manuel installation
---


### Requirements

---

- Docker / Docker-compose https://www.docker.com/
- git
- Terminal

 
### Installation script

---

```bash
mkdir debat-public
cd debat-public
git clone -q git@gitlab.com:debat-public/client-web.git
git clone -q git@gitlab.com:debat-public/infra.git
git clone -q git@gitlab.com:debat-public/server.git
git clone -q git@gitlab.com:debat-public/documentation.git
git clone -q git@gitlab.com:debat-public/css-debat-public.git
pwd=`pwd`;
make -C $pwd/infra dev_start
code . // To Open the project into Visual Studio Code
```