---
id: technos
title: Technologies used
sidebar_label: Technologies used
---

## Frontend 

- ### Typescript 
  The most loved web programming language, adopted by the majority of javascript developers for its ability to type javascript code. Thank's to it, VSCode is able to understand more code, and productivity of developer goes up as well.
- ### React 
  The best modern web javascript framework with the biggest community !
- ### Redux 
    We use redux to store the data that should be excessed within the whole app.
- ### Apollo 
   The industry standard GraphQL implementation that provides the data graph layer that connects the frontend to the backend.
- ### GraphQL 
    The language used to query of backend API.
- ### Axios 
    Used in this project for http requesting external API that don't use graphQL (42 for example)
- Oauth2 : Used to authenticate users with their 42 credentials.
- ### draftjs 
    Stlying and markdown module.
- ### D3js
    A powerful visualization library used in this project to render the France map.
- ### Bulma 
    We added Bulma to the classical "gouv" theme to be able to add more styling to our platform.

## Backend 

- ### Golang: 
    We have chosen go because it's a very powerful backend language who have better performance that javascript and simpler syntax than Rust. We could have chosen Deno, but it wasn't released when we started this project !
- ### Apollo 
    Just like in the frontend, it's the implementation of graphql that provides the graph for our forntend.
- ### GraphQL: 
    the language used by our backend to query and mutate the data in our database.
- ### graphql-go: 
    Actually... the golang community isn't that big, so that it wasn't as easy as for javascript to implement graphql, this is why we have chosen this library that is the common golang handler for graphql.
- ### Postgres:
    The World's Most Advanced Open Source Relational      Database.

## Infrastructure 

- ### Kubernetes 
    to handle container.
- ### Docker 
    to launch instances of our frontend and backend within a common virtual machine. Docker prevents from the classical "but it was working on my machine"
- ### Google Cloud Infra 
    A solid infrastructure made by Google that is also free to use !
- ### Gitlab CICD Pipeline 
    Continuous Integration and Deployment handled in Gitlab —> This was, the app is automatically deployed in our GC Infra each time we have new code merged in the master branch.