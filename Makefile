################################################################################
#   DOCKER
################################################################################

# Build
docker_build:
	docker build -t registry.gitlab.com/debat-public/documentation .

# Build and launch Docker with the app
docker_run: docker_stop docker_rm docker_build
	docker run -d -p 3000:3000 --name documentation_container -e REACT_APP_SERVER_URL=http://localhost:8383 registry.gitlab.com/debat-public/documentation:latest

docker_exec_shell:
	docker exec -it documentation_container /bin/bash

# Stop container
docker_stop:
	docker stop documentation_container || true

# Remove container
docker_rm:
	docker rm documentation_container || true

# Stop all docker containers
docker_fstop:
	docker stop $(docker ps -a -q)

# Build and Push the image to the gitlab registry
docker_push: docker_build docker_login
	docker push registry.gitlab.com/debat-public/documentation

# Login docker with registry.gitlab.com
docker_login:
	docker login registry.gitlab.com

# Remove all docker containers and images from the machine (add -f to force delete)
docker_fclean:
	docker rm $(docker ps -a -q)
	docker rmi $(docker images -q)