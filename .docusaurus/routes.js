
import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';

export default [
  
{
  path: '/',
  component: ComponentCreator('/'),
  exact: true,
  
},
{
  path: '/blog',
  component: ComponentCreator('/blog'),
  exact: true,
  
},
{
  path: '/blog/tags',
  component: ComponentCreator('/blog/tags'),
  exact: true,
  
},
{
  path: '/blog/tags/beta',
  component: ComponentCreator('/blog/tags/beta'),
  exact: true,
  
},
{
  path: '/blog/tags/hello',
  component: ComponentCreator('/blog/tags/hello'),
  exact: true,
  
},
{
  path: '/blog/welcome',
  component: ComponentCreator('/blog/welcome'),
  exact: true,
  
},
{
  path: '/docs/:route',
  component: ComponentCreator('/docs/:route'),
  
  routes: [
{
  path: '/docs/accessibilite',
  component: ComponentCreator('/docs/accessibilite'),
  exact: true,
  
},
{
  path: '/docs/bdd',
  component: ComponentCreator('/docs/bdd'),
  exact: true,
  
},
{
  path: '/docs/client-web',
  component: ComponentCreator('/docs/client-web'),
  exact: true,
  
},
{
  path: '/docs/code-of-conduct',
  component: ComponentCreator('/docs/code-of-conduct'),
  exact: true,
  
},
{
  path: '/docs/comment-contribuer',
  component: ComponentCreator('/docs/comment-contribuer'),
  exact: true,
  
},
{
  path: '/docs/configuration',
  component: ComponentCreator('/docs/configuration'),
  exact: true,
  
},
{
  path: '/docs/contributing-open-source',
  component: ComponentCreator('/docs/contributing-open-source'),
  exact: true,
  
},
{
  path: '/docs/css-debat-public',
  component: ComponentCreator('/docs/css-debat-public'),
  exact: true,
  
},
{
  path: '/docs/design-principles',
  component: ComponentCreator('/docs/design-principles'),
  exact: true,
  
},
{
  path: '/docs/doc-docusaurus',
  component: ComponentCreator('/docs/doc-docusaurus'),
  exact: true,
  
},
{
  path: '/docs/documentation',
  component: ComponentCreator('/docs/documentation'),
  exact: true,
  
},
{
  path: '/docs/gitlab',
  component: ComponentCreator('/docs/gitlab'),
  exact: true,
  
},
{
  path: '/docs/go-backend',
  component: ComponentCreator('/docs/go-backend'),
  exact: true,
  
},
{
  path: '/docs/infra',
  component: ComponentCreator('/docs/infra'),
  exact: true,
  
},
{
  path: '/docs/installation',
  component: ComponentCreator('/docs/installation'),
  exact: true,
  
},
{
  path: '/docs/introduction',
  component: ComponentCreator('/docs/introduction'),
  exact: true,
  
},
{
  path: '/docs/makefile',
  component: ComponentCreator('/docs/makefile'),
  exact: true,
  
},
{
  path: '/docs/manifeste',
  component: ComponentCreator('/docs/manifeste'),
  exact: true,
  
},
{
  path: '/docs/markdown',
  component: ComponentCreator('/docs/markdown'),
  exact: true,
  
},
{
  path: '/docs/technos',
  component: ComponentCreator('/docs/technos'),
  exact: true,
  
},
{
  path: '/docs/termes-conditions',
  component: ComponentCreator('/docs/termes-conditions'),
  exact: true,
  
}],
},
  
  {
    path: '*',
    component: ComponentCreator('*')
  }
];
